<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Songs Comparison</title>
	<link rel="icon" href="<?php echo base_url(); ?>assets/icon/icon.png">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/my.css">
	<link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>

	<script type = "text/javascript" >
		function preventBack(){window.history.forward();}
		setTimeout("preventBack()", 0);
		window.onunload=function(){null};	
	</script>
	
	<script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">
	
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta name="robots" content="noindex, nofollow">
	<meta name="googlebot" content="noindex, nofollow">
	
    <script type="text/javascript" src="//code.jquery.com/jquery-3.1.0.js"></script>
<!--<link rel="stylesheet" type="text/css" href="/css/result-light.css"> -->
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
    <script type="text/javascript" src="https://download.affectiva.com/js/3.1/affdex.js"></script>
</head>
	
<?php echo validation_errors(); ?>
<?php echo form_open('data/ratings', array('onsubmit' => 'return checkTime()')); ?>

<body ontouchstart="">

<input type="hidden" name="rnd1" value="<?php echo $rnd1; ?>">
<input type="hidden" name="rnd2" value="<?php echo $rnd2; ?>">

<h1>Songs</h1>
<p1>
Listen to these two song snippets and imagine that you are working in your office. Which one do you prefer
to listen? Express your preference towards the song on the left or on the right by moving the slider below to its direction.
<br> 
</p1>
<br>
<table>
  <tr>
	<th style="width:10%">		
		<div align="left">					
		<button type="button" id="pButton1" class="play" onclick="playSong1()"></button>
		<div style="font-size:0.9vw"><span id="elapsed1">0</span> / 10 sec</div>
		<div>
	</th>
	<th style="width:80%">		
		<div align="center">
		<input type="range" name="rating" align="middle"  max="2", min="-2" value=0 oninput="showValue(this.value)"/>
		<br><br>

	</th>
	<th style="width:10%">	
		<div align="right">				
		<button type="button" id="pButton2" class="play" onclick="playSong2()"></button>
		<div style="font-size:0.9vw"><span id="elapsed2">0</span> / 10 sec</div>
		</div>
	</th>
  </tr>
 </table>
<div  align="center">
<p style="font-size:2.3vw; text-align:center;"><span id="range">Both songs are about the same</span></p>
</div>
<br><br>
<button  type="submit" name="submit">Submit Rating</button>
<br><br>

<?php

$session_id_for_js = "'".session_id()."'";

#$this->session->been_alerted is set to 0 in the Pages.php's personal class.
if ($this->session->been_alerted == 0 and $this->session->rated_songs >= 2){
$this->session->been_alerted = 1;
echo '	<script>
			swal("Great!", "While we still have some interesting questions for you, you can also keep rating songs and proceed to next part of the survey whenever you want by clicking the Im Done Rating button at the bottom of the screen.", "warning");	
		</script>	
	';
}

if ($this->session->rated_songs >= 2){
echo "<button onclick='warnBeforeRedirect()' type='button'>I'm Done Rating</button>";		
}

?>	
<script>
		var audioSource1 = new Audio('<?php echo base_url(); ?>assets/<?php echo $song1; ?>');
		var audioSource2 = new Audio('<?php echo base_url(); ?>assets/<?php echo $song2; ?>');
		
		var maxTimeSong1 = 0;
		var maxTimeSong2 = 0;
		function TrackAudio1(element){
		if (maxTimeSong1 <= Math.floor(element.currentTime))
		{maxTimeSong1 =  Math.floor(element.currentTime);
		}
		   //Value in seconds.
		}
		function TrackAudio2(element){
		if (maxTimeSong2 <= Math.floor(element.currentTime))
		{maxTimeSong2 =  Math.floor(element.currentTime);
		}
		   //Value in seconds.
		}

		function checkTime(){
		if ((audioSource1.currentTime>=2 || audioSource1.ended) && (audioSource2.currentTime>=2 || audioSource2.ended)){
		return true;
		}else
		swal("Please!", "Listen to both song snippets carefully before submitting your rating! Thanks!", "error");	
		return false;
		}
		
		function myFunction() {
			var txt;
			var r = confirm("Are you sure you are done rating?");
			if (r == true) {
				window.location.href = '<?php echo base_url();?>index.php/pages/ffm';
				
			}
			
		}
		var sliderValue;
		
		function showValue(newValue){
		sliderValue = newValue;
		
		switch(newValue) {
		case "-2":
			document.getElementById("range").innerHTML="The song on the left is ABSOLUTELY better";
        break;
		case "-1":
			document.getElementById("range").innerHTML="The song on the left is SOMEWHAT better";
        break;
		case "0":
			document.getElementById("range").innerHTML="Both songs are about the same";
        break;
		case "1":
			document.getElementById("range").innerHTML="The song on the right is SOMEWHAT better";
        break;
		case "2":
			document.getElementById("range").innerHTML="The song on the right is ABSOLUTELY better";
        break;
		default:
			document.getElementById("range").innerHTML="Both songs are about the same";
		}
		
		
		}
		

function playSong1(){
	if (audioSource1.paused) {
		audioSource1.play();
		// remove play, add pause
		audioSource2.pause();
		pButton1.className = "";
		pButton1.className = "pause";
		pButton2.className = "";
		pButton2.className = "play";
	} else { // pause audio2
		audioSource1.pause();
		// remove pause, add play
		pButton1.className = "";
		pButton1.className = "play";
	}
	}

audioSource1.ontimeupdate = function showElapsedSource1(){
document.getElementById("elapsed1").innerHTML=Math.floor(audioSource1.currentTime);
document.getElementById("total1").innerHTML=Math.floor(audioSource1.duration);
TrackAudio1(this);
}

audioSource1.onended = function pauseToPlayButton1(){pButton1.className = ""; pButton1.className = "play";}

function playSong2(){
	if (audioSource2.paused) {
		audioSource2.play();
		audioSource1.pause();
		// remove play, add pause
		pButton2.className = "";
		pButton2.className = "pause";
		pButton1.className = "";
		pButton1.className = "play";
	} else { // pause audio2
		audioSource2.pause();
		// remove pause, add play
		pButton2.className = "";
		pButton2.className = "play";
	}
	}

audioSource2.ontimeupdate = function showElapsedSource2(){
document.getElementById("elapsed2").innerHTML=Math.floor(audioSource2.currentTime);
document.getElementById("total2").innerHTML=Math.floor(audioSource2.duration);
TrackAudio1(this);
}

audioSource2.onended = function pauseToPlayButton2(){pButton2.className = ""; pButton2.className = "play";}

  function warnBeforeRedirect() {
    swal({
      title: "Are you sure?", 
      text: "Are you sure you want to continue to the next part of the survey?", 
      type: "warning",
      showCancelButton: true,
	  confirmButtonText: "Yes!",
	  cancelButtonText: "No! I wanna rate more songs.",
	  confirmButtonColor: "#ec6c62"
    }, function() {
      // Redirect the user
		window.location.href = '<?php echo base_url();?>index.php/pages/ffm';      
    });
  }
</script>
<p id="demo"></p>
					Progress:<br><br>
			<div id="progress">
			</div><br><?php echo $this->session->pagenum*10 ."%"?><br>
				  </form>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
			<style>
			#progress {
				background: #ffffff;
				border-radius: 25px;
				height: 40px;
				width: 100%;
				padding: 3px;
			}

			#progress:after {
				content: '';
				display: block;
				background: #5fcf80;
				width: <?php echo $this->session->pagenum*10 ."%"?>;
				height: 100%;
				border-radius: 25px;
			}
			</style>

	  
		<?php echo $this->session->been_alerted; ?> <br>
       <?php echo "current IPv6 is  ",$this->session->ip; ?> <br>
	  <?php echo "number of songs rated are:  ",$this->session->rated_songs; ?><br>
	  <?php echo "time spent:" . $this->session->starttime ?>
	  
	 

  <span id="emotions"></span>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8" id="affdex_elements" style="display:none; width:680px;height:480px;"></div>
      <div class="col-md-4">
        <div style="height:25em;">
          <!--<strong>EMOTION TRACKING RESULTS</strong>
          <div id="results" style="word-wrap:break-word;"></div>
        </div>
        <div>
          <strong>DETECTOR LOG MSGS</strong>
        </div>
        <div id="logs"></div>
      </div>
    </div>-->
    <div>
      <button id="stop" onclick="onStop()">Stop Video Feed (debug)</button>
  </div>


<script type="text/javascript">//<![CDATA[

      // SDK Needs to create video and canvas nodes in the DOM in order to function
      // Here we are adding those nodes a predefined div.
      var divRoot = $("#affdex_elements")[0];
      var width = 640;
      var height = 480;
      var faceMode = affdex.FaceDetectorMode.LARGE_FACES;
      //Construct a CameraDetector and specify the image width / height and face detector mode.
      var detector = new affdex.CameraDetector(divRoot, width, height, faceMode);

      //Enable detection of all Expressions, Emotions and Emojis classifiers.
      detector.detectAllEmotions();
      detector.detectAllExpressions();
      detector.detectAllEmojis();
      detector.detectAllAppearance();

      //Add a callback to notify when the detector is initialized and ready for runing.
      detector.addEventListener("onInitializeSuccess", function() {
        log('#logs', "Initialized");
        //Display canvas instead of video feed because we want to draw the feature points on it
        $("#face_video_canvas").css("display", "block");
        $("#face_video").css("display", "none");
      });

      function log(node_name, msg) {
        $(node_name).append("<span>" + msg + "</span><br />")
        //$.get(
          //  "storeEmotions.php",
            //{message: msg}, 
            //function(data,status){},
            //"html");
      }

      //function executes when Start button is pushed.
      function onStart() {
        if (detector && !detector.isRunning) {
          $("#logs").html("");
          detector.start();
        }
        log('#logs', "Started");
      }

      //function executes when the Stop button is pushed.
      function onStop() {
        log('#logs', "Ended");
        if (detector && detector.isRunning) {
          detector.removeEventListener();
          detector.stop();
        }
      };

      //function executes when the Reset button is pushed.
      function onReset() {
        log('#logs', "Reset");
        if (detector && detector.isRunning) {
          detector.reset();

          $('#results').html("");
        }
      };

      //Add a callback to notify when camera access is allowed
      detector.addEventListener("onWebcamConnectSuccess", function() {
        log('#logs', "Allowed");
      });

      //Add a callback to notify when camera access is denied
      detector.addEventListener("onWebcamConnectFailure", function() {
        log('#logs', "Denied");
        console.log("Webcam access denied");
      });

      //Add a callback to notify when detector is stopped
      detector.addEventListener("onStopSuccess", function() {
        log('#logs', "SuccessStop");
        $("#results").html("");
      });

      //Add a callback to receive the results from processing an image.
      //The faces object contains the list of the faces detected in an image.
      //Faces object contains probabilities for all the different expressions, emotions and appearance metrics
      var counter = 1;
	  detector.addEventListener("onImageResultsSuccess", function(faces, image, timestamp) {
        
        if (faces.length > 0) {
		  
		  counter = counter+1;
		  
		  jsonEmotions = JSON.stringify(faces[0].emotions);
		  var parsedEmotions = JSON.parse(jsonEmotions);
		  
		  jsonAppearance = JSON.stringify(faces[0].appearance);
		  var parsedAppearance = JSON.parse(jsonAppearance);
		  
		  jsonExpressions = JSON.stringify(faces[0].expressions);
		  var parsedExpressions = JSON.parse(jsonExpressions);
		  
		  var _gender;
		  _gender[1] = parsedAppearance.gender;
		  var _glasses = parsedAppearance.glasses;
		  var _age = parsedAppearance.age;
		  var _ethnicity = parsedAppearance.ethnicity;
		  
		  var _smile = parsedExpressions.smile;
		  var _innerBrowRise = parsedExpressions.innerBrowRise;
		  var _browRise = parsedExpressions.browRise;
		  var _browFurrow = parsedExpressions.browFurrow;
		  var _noseWrinkle = parsedExpressions.noseWrinkle;
		  var _upperLipRaise = parsedExpressions.upperLipRaise;
		  var _lipCornerDepressor = parsedExpressions.lipCornerDepressor;
		  var _chinRaise = parsedExpressions.chinRaise;
		  var _lipPucker = parsedExpressions.lipPucker;
		  var _lipPress = parsedExpressions.lipPress;
		  var _lipSuck = parsedExpressions.lipSuck;
		  var _mouthOpen = parsedExpressions.mouthOpen;
		  var _smirk = parsedExpressions.smirk;
		  var _eyeCloser = parsedExpressions.eyeCloser;
		  var _attention = parsedExpressions.attention;
		  var _lidTighten = parsedExpressions.lidTighten;
		  var _jawDrop = parsedExpressions.jawDrop;
		  var _dimpler = parsedExpressions.dimpler;
		  var _eyeWiden = parsedExpressions.eyeWiden;
		  var _cheekRaise = parsedExpressions.cheekRaise;
		  var _lipStretch = parsedExpressions.lipStretch;
		  
		  var _faces = faces.length;
		  var _joy = Math.round(parsedEmotions.joy);
		  var _sadness = Math.round(parsedEmotions.sadness);
		  var _disgust = Math.round(parsedEmotions.disgust);
		  var _contempt = Math.round(parsedEmotions.contempt);
		  var _anger = Math.round(parsedEmotions.anger);
		  var _fear = Math.round(parsedEmotions.fear);
		  var _surprise = Math.round(parsedEmotions.surprise);
		  var _valence = Math.round(parsedEmotions.valence);
		  var _engagement = Math.round(parsedEmotions.engagement);
		  
		  var _session_id = <?php echo $session_id_for_js; ?>;
		  var _song1 = <?php echo $rnd1; ?>;
		  var _song2 = <?php echo $rnd2; ?>;
		  var _play1 = !audioSource1.paused;
		  var _play2 = !audioSource1.paused;
		  var _pause1 = audioSource1.paused;
		  var _pause2 = audioSource1.paused;
		  var _sliderValue = sliderValue;
		  
		  var _emoji = faces[0].emojis.dominantEmoji;
		  
		  console.log(_gender[1]);
		  
		    $.get(
            "<?php echo base_url(); ?>assets/emotions/storeEmotions.php",
            {	/*
				session_id: _session_id,
				song1: _song1,
				song2: _song2,
				play1: _play1,
				play2: _play2,
				pause1: _pause1,
				pause2: _pause2,
				sliderValue: _sliderValue,
				
				time: timestamp.toFixed(2),
				faces: _faces,
				joy: _joy,
				sadness: _sadness,
				disgust: _disgust,
				contempt: _contempt,
				anger: _anger,
				fear: _fear,
				surprise: _surprise,
				valence: _valence,
				engagement: _engagement,
				*/
				emotions: parsedEmotions
			}, 
            function(data,status){},
            "html");
        }
      });
	 /* 
	  function sendToPHP(){
				    $.get(
            "<?php echo base_url(); ?>assets/emotions/storeEmotions.php",
            {	/*
				session_id: _session_id,
				song1: _song1,
				song2: _song2,
				play1: _play1,
				play2: _play2,
				pause1: _pause1,
				pause2: _pause2,
				sliderValue: _sliderValue,
				
				time: timestamp.toFixed(2),
				faces: _faces,
				joy: _joy,
				sadness: _sadness,
				disgust: _disgust,
				contempt: _contempt,
				anger: _anger,
				fear: _fear,
				surprise: _surprise,
				valence: _valence,
				engagement: _engagement,
				
				emotions: parsedEmotions
			}, 
            function(data,status){},
            "html");
	  }
*/
	window.onload = onStart();
	window.onbeforeunload =onStop();
</script>

  <script>
  // tell the embed parent frame the height of the content
  if (window.parent && window.parent.parent){
    window.parent.parent.postMessage(["resultsFrame", {
      height: document.body.getBoundingClientRect().height,
      slug: "opyh5e8d"
    }], "*")
  }
</script>	
	
		 
</body>	
</html>