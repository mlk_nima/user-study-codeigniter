  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Five Factor Model Questions</title>
		<link rel="icon" href="<?php echo base_url(); ?>assets/icon/icon.png">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mycss.css">
        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
		
		<script type = "text/javascript" >
		   function preventBack(){window.history.forward();}
			setTimeout("preventBack()", 0);
			window.onunload=function(){null};
		</script>		
        	<script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">

    </head>
    <body>

	<?php echo validation_errors(); ?>
	<?php echo form_open('data/ffm', array('onsubmit' => 'return checkRadios()')); ?>
	

	
        <h1 align=left>Survey on Five Factor Model</h1>
			
			<p1>
			<?php include('./././assets/questions/questionsffm.php'); echo $title?>
			<br>
			</p1>
			<br>
        <fieldset id="question1">
          <legend><span class="number">?</span><em>I See Myself As...</em></legend><br>
			
          <label><?php include('./././assets/questions/questionsffm.php'); echo $q1?><br>
		  </label>
          <input type="radio" id="q1" value="AgreeStrongly" name="q1"><label for="q1" class="light">Agree Strongly</label><br>
          <input type="radio" id="q1" value="AgreeModerately" name="q1"><label for="q1" class="light">Agree Moderately</label><br>
          <input type="radio" id="q1" value="AgreeALitte" name="q1"><label for="q1" class="light">Agree A Litte</label><br>
		  <input checked type="radio" id="q1" value="Nuetral" name="q1"><label for="q1" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q1" value="DisagreeALitte" name="q1"><label for="q1" class="light">Disagree A Little</label><br>          
          <input type="radio" id="q1" value="DisagreeModerately" name="q1"><label for="q1" class="light">Disagree Moderately</label><br>
		  <input type="radio" id="q1" value="DisagreeStrongly" name="q1"><label for="q1" class="light">Disagree Strongly</label><br>
		  
		 </fieldset>
		 <fieldset id="question2">
          <label><?php include('./././assets/questions/questionsffm.php'); echo $q2?>
		  </label>
          <input type="radio" id="q2" value="AgreeStrongly" name="q2"><label for="q2" class="light">Agree Strongly</label><br>
          <input type="radio" id="q2" value="AgreeModerately" name="q2"><label for="q2" class="light">Agree Moderately</label><br>
          <input type="radio" id="q2" value="AgreeALitte" name="q2"><label for="q2" class="light">Agree A Litte</label><br>
		  <input checked type="radio" id="q2" value="Nuetral" name="q2"><label for="q2" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q2" value="DisagreeALitte" name="q2"><label for="q2" class="light">Disagree A Little</label><br>          
          <input type="radio" id="q2" value="DisagreeModerately" name="q2"><label for="q2" class="light">Disagree Moderately</label><br>
		  <input type="radio" id="q2" value="DisagreeStrongly" name="q2"><label for="q2" class="light">Disagree Strongly</label><br>
		  </fieldset>
		  
		  <fieldset id="question3">
		  <label><?php include('./././assets/questions/questionsffm.php'); echo $q3?>
		  </label>
          <input type="radio" id="q3" value="AgreeStrongly" name="q3"><label for="q3" class="light">Agree Strongly</label><br>
          <input type="radio" id="q3" value="AgreeModerately" name="q3"><label for="q3" class="light">Agree Moderately</label><br>
          <input type="radio" id="q3" value="AgreeALitte" name="q3"><label for="q3" class="light">Agree A Litte</label><br>
		  <input checked type="radio" id="q3" value="Nuetral" name="q3"><label for="q3" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q3" value="DisagreeALitte" name="q3"><label for="q3" class="light">Disagree A Little</label><br>          
          <input type="radio" id="q3" value="DisagreeModerately" name="q3"><label for="q3" class="light">Disagree Moderately</label><br>
		  <input type="radio" id="q3" value="DisagreeStrongly" name="q3"><label for="q3" class="light">Disagree Strongly</label><br>

		  </fieldset>
		  <fieldset>
		  <label><?php include('./././assets/questions/questionsffm.php'); echo $q4?>
		  </label>
          <input type="radio" id="q4" value="AgreeStrongly" name="q4"><label for="q4" class="light">Agree Strongly</label><br>
          <input type="radio" id="q4" value="AgreeModerately" name="q4"><label for="q4" class="light">Agree Moderately</label><br>
          <input type="radio" id="q4" value="AgreeALitte" name="q4"><label for="q4" class="light">Agree A Litte</label><br>
		  <input checked type="radio" id="q4" value="Nuetral" name="q4"><label for="q4" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q4" value="DisagreeALitte" name="q4"><label for="q4" class="light">Disagree A Little</label><br>          
          <input type="radio" id="q4" value="DisagreeModerately" name="q4"><label for="q4" class="light">Disagree Moderately</label><br>
		  <input type="radio" id="q4" value="DisagreeStrongly" name="q4"><label for="q4" class="light">Disagree Strongly</label><br>

		  </fieldset>
		  <fieldset>
		  <label><?php include('./././assets/questions/questionsffm.php'); echo $q5?>
		  </label>
          <input type="radio" id="q5" value="AgreeStrongly" name="q5"><label for="q5" class="light">Agree Strongly</label><br>
          <input type="radio" id="q5" value="AgreeModerately" name="q5"><label for="q5" class="light">Agree Moderately</label><br>
          <input type="radio" id="q5" value="AgreeALitte" name="q2"><label for="q5" class="light">Agree A Litte</label><br>		  
		  <input checked type="radio" id="q5" value="Nuetral" name="q5"><label for="q5" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q5" value="DisagreeALitte" name="q5"><label for="q5" class="light">Disagree A Little</label><br>          

          <input type="radio" id="q5" value="DisagreeModerately" name="q5"><label for="q5" class="light">Disagree Moderately</label><br>
		  <input type="radio" id="q5" value="DisagreeStrongly" name="q5"><label for="q5" class="light">Disagree Strongly</label><br>

		  </fieldset>
		  <fieldset>
		  <label><?php include('./././assets/questions/questionsffm.php'); echo $q6?>
		  </label>
          <input type="radio" id="q6" value="AgreeStrongly" name="q6"><label for="q6" class="light">Agree Strongly</label><br>
          <input type="radio" id="q6" value="AgreeModerately" name="q6"><label for="q6" class="light">Agree Moderately</label><br>
          <input type="radio" id="q6" value="AgreeALitte" name="q6"><label for="q6" class="light">Agree A Litte</label><br>		  
		  <input checked type="radio" id="q6" value="Nuetral" name="q6"><label for="q6" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q6" value="DisagreeALitte" name="q6"><label for="q6" class="light">Disagree A Little</label><br>          
          <input type="radio" id="q6" value="DisagreeModerately" name="q6"><label for="q6" class="light">Disagree Moderately</label><br>
		  <input type="radio" id="q6" value="DisagreeStrongly" name="q6"><label for="q6" class="light">Disagree Strongly</label><br>

		  </fieldset>
		  <fieldset>
		  <label><?php include('./././assets/questions/questionsffm.php'); echo $q7?>
		  </label>
          <input type="radio" id="q7" value="AgreeStrongly" name="q7"><label for="q7" class="light">Agree Strongly</label><br>
          <input type="radio" id="q7" value="AgreeModerately" name="q7"><label for="q7" class="light">Agree Moderately</label><br>
          <input type="radio" id="q7" value="AgreeALitte" name="q7"><label for="q7" class="light">Agree A Litte</label><br>
		  <input checked type="radio" id="q7" value="Nuetral" name="q7"><label for="q7" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q7" value="DisagreeALitte" name="q7"><label for="q7" class="light">Disagree A Little</label><br>          
          <input type="radio" id="q7" value="DisagreeModerately" name="q7"><label for="q7" class="light">Disagree Moderately</label><br>
		  <input type="radio" id="q7" value="DisagreeStrongly" name="q7"><label for="q7" class="light">Disagree Strongly</label><br>

		  </fieldset>
		  <fieldset>
		  <label><?php include('./././assets/questions/questionsffm.php'); echo $q8?>
		  </label>
          <input type="radio" id="q8" value="AgreeStrongly" name="q8"><label for="q8" class="light">Agree Strongly</label><br>
          <input type="radio" id="q8" value="AgreeModerately" name="q8"><label for="q8" class="light">Agree Moderately</label><br>
          <input type="radio" id="AgreeALitte" value="AgreeALitte" name="q8"><label for="q8" class="light">Agree A Litte</label><br>
		  <input checked type="radio" id="q8" value="Nuetral" name="q8"><label for="q8" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q8" value="DisagreeALitte" name="q8"><label for="q8" class="light">Disagree A Little</label><br>          
          <input type="radio" id="q8" value="DisagreeModerately" name="q8"><label for="q8" class="light">Disagree Moderately</label><br>
		  <input type="radio" id="q8" value="DisagreeStrongly" name="q8"><label for="q8" class="light">Disagree Strongly</label><br>

		  </fieldset>
		  <fieldset>
		  <label><?php include('./././assets/questions/questionsffm.php'); echo $q9?>
		  </label>
          <input type="radio" id="q9" value="AgreeStrongly" name="q9"><label for="q9" class="light">Agree Strongly</label><br>
          <input type="radio" id="q9" value="AgreeModerately" name="q9"><label for="q9" class="light">Agree Moderately</label><br>
          <input type="radio" id="q9" value="AgreeALitte" name="q9"><label for="q9" class="light">Agree A Litte</label><br>
		  <input checked type="radio" id="q9" value="Nuetral" name="q9"><label for="q9" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q9" value="DisagreeALitte" name="q9"><label for="q9" class="light">Disagree A Little</label><br>          
		  <input type="radio" id="q9" value="DisagreeModerately" name="q9"><label for="q9" class="light">Disagree Moderately</label><br>
		  <input type="radio" id="q9" value="DisagreeStrongly" name="q9"><label for="q9" class="light">Disagree Strongly</label><br>

		  </fieldset>
		  <fieldset>
		  <label><?php include('./././assets/questions/questionsffm.php'); echo $q10?>
		  </label>
          <input type="radio" id="q10" value="AgreeStrongly" name="q10"><label for="q10" class="light">Agree Strongly</label><br>
          <input type="radio" id="q10" value="AgreeModerately" name="q10"><label for="q10" class="light">Agree Moderately</label><br>
          <input type="radio" id="q10" value="AgreeALitte" name="q10"><label for="q10" class="light">Agree A Litte</label><br>
		  <input type="radio" id="q10" value="Nuetral" name="q10"><label for="q10" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q10" value="DisagreeALitte" name="q10"><label for="q10" class="light">Disagree A Little</label><br>
          <input type="radio" id="q10" value="DisagreeModerately" name="q10"><label for="q10" class="light">Disagree Moderately</label><br>
		  <input type="radio" id="q10" value="DisagreeStrongly" name="q10"><label for="q10" class="light">Disagree Strongly</label><br>

		  </fieldset>
	  
		  
        
        <button  TYPE="submit" name="submit">Next Step</button>
		<br><br>
					Progress:<br><br>
			<div id="progress">
			</div><br><?php echo $this->session->pagenum*10 ."%"?><br>
				  </form>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
			<style>
			#progress {
				background: #ffffff;
				border-radius: 25px;
				height: 40px;
				width: 100%;
				padding: 3px;
			}

			#progress:after {
				content: '';
				display: block;
				background: #5fcf80;
				width: <?php echo $this->session->pagenum*10 ."%"?>;
				height: 100%;
				border-radius: 25px;
			}
			</style>
	  <button onclick="checkRadios();">check "debug"</button>	
	<script type="text/javascript">
	
	var radioGroupsCount = 10;
	var radioGroups = ["q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8", "q9", "q10"];

	function checkRadios()
	{
		
		var flag = true;
		
		for (var j = 0; j < 10; j++)
		{	
			var radios = document.getElementsByName(radioGroups[j])
			
			isThisChecked = 0;
			for (var i = 0; i < radios.length; i++) {
				
				if(radios[i].checked)
				{
				isThisChecked++;	
				}
				
			}
			console.log(isThisChecked);
			if (! isThisChecked)
			{flag=false;}
		}
		
		console.log("Are all radios checked? " + flag);
		
		if (flag == false)
			{swal("Please!", "Answer to all questions before proceeding!", "error");}
		return flag;
	}
	</script>
		
    <?php echo "current IPv6 is  ",$this->session->ip; ?> <br>
	<?php echo "number of songs rated are:  ",$this->session->rated_songs; ?>
	</body>
</html>