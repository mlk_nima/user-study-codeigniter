  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Personal Information</title>
		<link rel="icon" href="<?php echo base_url(); ?>assets/icon/icon.png">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mycss.css">
        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>

		
	<meta name="viewport" content="width=device-width, initial-scale=1">
        
    </head>
	
	<?php echo validation_errors(); ?>
	<?php echo form_open('data/personal'); ?>
	
    <body>

		<input type="hidden" id="valence" name="valence" value="0">
		<input type="hidden" id="arousal" name="arousal" value="0">

        <h1>Survey</h1>
			
			<p1>
			First of all, please help us proceed by providing some of your basic information.<br> 
			</p1>
			<br>
        <fieldset>
          <legend><span class="number">?</span>Your basic info</legend>
          <label for="age_id">* How old are you? Please use a single number.</label>
          <input type="number" value="20" min=15 max=100 id="age_id" name="user_age" autofocus>
		</fieldset>
		
        <fieldset>
        <label for="sex">* How do you identify your gender?</label>
        <select id="sex_id" name="user_sex">
            <option value="Female">Female</option>
            <option value="Male">Male</option>
            <option value="Other">Other / Prefer not to disclose</option>
        </select>
        </fieldset>        
		
        <fieldset>
        <label for="edu">* Please specify your educational level.</label>
        <select id="edu_id" name="user_edu">
            <option value="HighSchool">High School</option>
            <option value="Bachelor">Bachelor's Degree</option>
            <option value="Master">Master's Degree</option>
            <option value="PhD"> PhD degree</option>
            <option value="PostDoc">Post-doc or Above</option>
        </select>
        </fieldset>

        <fieldset>
        <label for="english">* Please rate your ability to read and understand textual information in English.</label>
        <select id="english_id" name="user_english">
            <option value="VeryPoor">Very Poor</option>
            <option value="Poor">Poor</option>
            <option selected value="Fair">Fair</option>
            <option value="Good">Good</option>
            <option value="VeryGood">Very Good</option>
        </select>
        </fieldset>

        <fieldset>
        <label for="job">* In what sector do you currently work or have worked?</label>
        <select id="job_id" name="user_job">
<option value=F01 >Administration, Commerce and Information Technology</option>
<option value=F02 >Agriculture and Fishing</option>
<option value=F03 >Food and Tourism</option>
<option value=F04 >Arts</option>
<option value=F05 >Building and Public Works</option>
<option value=F06 >Wood and Related Materials</option>
<option value=F07 >Chemistry and Biology</option>
<option value=F08 >Communication and Documentation</option>
<option value=F09 >Leather, Textile and Clothing</option>
<option value=F10 >Law</option>
<option value=F11 >Electrical Engineering</option>
<option value=F12 >Motorized Equipment Maintenance</option>
<option value=F13 >Environment and Land Use Planning</option>
<option value=F14 >Études plurisectorielles</option>
<option value=F15 >Mechanical Production</option>
<option value=F16 >Forestry and Paper</option>
<option value=F17 >Literature</option>
<option value=F18 >Maintenance Mechanics</option>
<option value=F19 >Metallurgy</option>
<option value=F20 >Mining and Construction Site Work</option>
<option value=F21 >Healthcare</option>
<option value=F22 >Applied Sciences</option>
<option value=F23 >Administrative Sciences</option>
<option value=F24 >Health Sciences</option>
<option value=F25 >Education Sciences</option>
<option value=F26 >Human Sciences</option>
<option value=F27 >Pure Sciences</option>
<option value=F28 >Social, Educational and Legal Services</option>
<option value=F29 >Esthetic Services</option>
<option value=F30 >Transportation</option>
<option value=F31 >Others</option>
        </select>
        </fieldset>
        <legend><span class="number">?</span>How do you feel right now?</legend>
		Click the point in the square below to represent your feeling.<br><br>
		<fieldset>
		<table align="center" width=250px>
			<th align="left">
			Angry/Fearful
			</th>
			<th align="right">
			Joyful
			</th>
		</table>
			<div align="center">
				<canvas onclick="drawRect()" id="box" width="200" height="200"
				style="border:1px solid #4bc970; background:#ebf4ed;">
				</canvas>
			</div>
		<table align="center" width=250px>
			<th align="left">
			Sad
			</th>
			<th align="right">
			Glad
			</th>
		</table>			
			
		</fieldset>
		
		<fieldset>
          <legend><span class="number">t</span>Twitter</legend>		
          <label for="twitter_id">If you like, please write down your twitter account name. (eg: john_doe)</label>
          <input type="text" id="twitter_id" name="user_twitter" autofocus>
		</fieldset>
		
		<fieldset>
		  <legend><span class="number">f</span>Facebook</legend>
          <label for="facebook_id">If you like, please write down your facebook account name. (eg: john.doe.58)</label>
          <input type="text" id="facebook_id" name="user_facebook" autofocus>
		</fieldset>
		<button  TYPE="submit" name="submit">Next Step</button>
					<br><br>
					Progress:<br><br>
			<div id="progress">
			</div><br><?php echo $this->session->pagenum*10 ."%"?><br>
				  </form> 
</body>				  
			<style>
			#progress {
				background: #ffffff;
				border-radius: 25px;
				height: 40px;
				width: 100%;
				padding: 3px;
			}

			#progress:after {
				content: '';
				display: block;
				background: #5fcf80;
				width: <?php echo $this->session->pagenum*10 ."%"?>;
				height: 100%;
				border-radius: 25px;
			}
			</style>
      <?php echo "current IPv6 is  ",$this->session->ip; ?> <br>
      <?php echo "current session id is  ",session_id(); ?> <br>
	  <?php echo "number of songs rated are:  ",$this->session->rated_songs; ?>
	  <?php echo $this->session->starttime ?>
    		<script >

		   function preventBack(){window.history.forward();}
			setTimeout("preventBack()", 0);
			window.onunload=function(){null};
			
			var x = 0;
			var y = 0;
			
			window.onload= drawLines();
			
			function set(){
				document.getElementById("valence").value=x;
				document.getElementById("arousal").value=y;
			}
			
		function getCursorPosition(canvas) {
			var rect = canvas.getBoundingClientRect();
			x = event.clientX - rect.left;
			y = event.clientY - rect.top;
			//alert("x: " + x + " y: " + y);
		};
		
		function drawRect(){
			
			
			//alert("x: " + x + " y: " + y);
			var c = document.getElementById("box");
			var ctx = c.getContext("2d");
			//clean the last point and redraw lines
			//oldx = x-10;
			//oldy = y-10;
			ctx.clearRect(0,0,200,200);
			drawLines();
			
			//get points
			getCursorPosition(box);
			//drawRect
			ctx.fillStyle="#0b6826";
			ctx.beginPath();
			ctx.arc(x, y, 4, 4,3*Math.PI);
			ctx.fill();
			//ctx.fillRect(x, y, 4, 4);
			
			
			//ctx.stroke();
			x=x-100;
			y=y-100;
			y *= -1;
			//alert("x: " + x + " y: " + y);
			set();
		};

		function drawLines(){
			  var canvas = document.getElementById("box");
			  var context = canvas.getContext("2d");

			  context.beginPath();
			  context.moveTo(100, 0);
			  context.lineTo(100, 200);
			  context.strokeStyle="#0b6826";
			  context.stroke();
			  
			  context.beginPath();
			  context.moveTo(0, 100);
			  context.lineTo(200, 100);
			  
			  context.strokeStyle="#0b6826";
			  context.stroke();			
					};
		</script>
</html>