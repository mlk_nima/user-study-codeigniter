  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Start</title>
		<link rel="icon" href="<?php echo base_url(); ?>assets/icon/icon.png">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mycss.css">
        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
		<script type = "text/javascript" >
		   function preventBack(){window.history.forward();}
			setTimeout("preventBack()", 0);
			window.onunload=function(){null};
		</script>
        
    </head>
	
		
	<?php echo form_open('pages/personal'); ?>
	
    <body>

      <form action="index.html" method="post" width=800px>
      
        <h2>Hello!</h2>

<p1>
What is the relationship between music and our everyday activities? You can help us by taking about 7 minutes to complete the following survey.
<br>
This research is a collaboration between the Free University of Bolzano (Italy) and the University of Ljubljana (Slovenia). Participation is voluntary. All information is confidential and will only be viewed by the researchers. Only aggregated data will be reported in possible publications, meaning that your identity will remain confidential.
<br>
If you wish to take part in the study and are at least 18 years of age, please click the "I agree, start survey" button. By doing that, you agree to the following:<br>
<ul>
  <li>I have read the above information, and understand the nature and purpose of this research.</li>
  <li>I understand that my participation is voluntary and that I may withdraw at any time.</li>
  <li>I understand that the results of this study will be treated confidentially.</li>
  <li>The results will be reported only in summary form and I will not be identified individually.</li>
</ul>
Thanks for taking the time for this survey. In case of questions, feel free to contact Nima Maleki via <a href="mailto:nima.maleki@unibz.it">nima.maleki@unibz.it</a>.
<br>
</p1>
<br>
			
			
<button name="submit" type="submit" autofocus>I Agree, Start The Survey</button>
			
		
	  	</form>
      <?php echo "current IPv6 is  ",$this->session->ip; ?> <br>
	  <?php echo "current session id is  ",session_id(); ?> <br>
	  <?php echo "number of songs rated are:  ",$this->session->rated_songs; ?>
	</body>
	
 