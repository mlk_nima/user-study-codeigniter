  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Musical Sophistication Index Questions</title>
		<link rel="icon" href="<?php echo base_url(); ?>assets/icon/icon.png">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mycss.css">
        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
		
		<script type = "text/javascript" >
		   function preventBack(){window.history.forward();}
			setTimeout("preventBack()", 0);
			window.onunload=function(){null};
		</script>
        
			<script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">

    </head>
    <body>

	<?php echo validation_errors(); ?>
	<?php echo form_open('data/msi', array('onsubmit' => 'return checkRadios()')); ?>
	
        <h1 align=left>Survey on Musical Sophistication Index</h1>
			
			<p1>
				<?php include('./././assets/questions/questionsmsi.php'); echo $title?><br> 
			<br>
			</p1>
			<br>
			
			<?php 
			#include('./././assets/questions/questionsmsi.php');
			
		#questions 1 to 6	
			for($i = 1; $i<=6;$i++)
			{
			echo '
		  <fieldset id="question"'.$i.'>
          <label>'.$q[$i].'<br>
		  </label>
          <input type="radio" id="q'.$i.'" value="1" name="q'.$i.'"><label for="q'.$i.'" class="light">Completely Disagree</label><br>
          <input type="radio" id="q'.$i.'" value="2" name="q'.$i.'"><label for="q'.$i.'" class="light">Strongly Disagree</label><br>
          <input type="radio" id="q'.$i.'" value="3" name="q'.$i.'"><label for="q'.$i.'" class="light">Disagree</label><br>
		  <input checked type="radio" id="q'.$i.'" value="4" name="q'.$i.'"><label for="q'.$i.'" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q'.$i.'" value="5" name="q'.$i.'"><label for="q'.$i.'" class="light">Agree</label><br>          
          <input type="radio" id="q'.$i.'" value="6" name="q'.$i.'"><label for="q'.$i.'" class="light">Strongly Agree</label><br>
		  <input type="radio" id="q'.$i.'" value="7" name="q'.$i.'"><label for="q'.$i.'" class="light">Completely Agree</label><br>
		  
		 </fieldset>
			';}
		
		#question 7
		echo '
		  <fieldset id="question7">
          <label>'.$q[7].'<br>
		  </label>
          <input type="radio" id="q7" value="0" name="q7"><label for="q7" class="light">0</label><br>
          <input type="radio" id="q7" value="1" name="q7"><label for="q7" class="light">1</label><br>
          <input type="radio" id="q7" value="2" name="q7"><label for="q7" class="light">2</label><br>
		  <input checked type="radio" id="q7" value="3" name="q7"><label for="q7" class="light">3</label><br>
          <input type="radio" id="q7" value="4to5" name="q7"><label for="q7" class="light">4 to 5</label><br>          
          <input type="radio" id="q7" value="6to9" name="q7"><label for="q7" class="light">6 to 9</label><br>
		  <input type="radio" id="q7" value="10plus" name="q7"><label for="q7" class="light">10 or more</label><br>
		  
		 </fieldset>
			';
		#question 8
		echo '
		  <fieldset id="question8">
          <label>'.$q[8].'<br>
		  </label>
          <input type="radio" id="q8" value="0" name="q8"><label for="q8" class="light">0</label><br>
          <input type="radio" id="q8" value="0.5" name="q8"><label for="q8" class="light">1</label><br>
          <input type="radio" id="q8" value="1" name="q8"><label for="q8" class="light">2</label><br>
		  <input checked type="radio" id="q8" value="2" name="q8"><label for="q8" class="light">3</label><br>
          <input type="radio" id="q8" value="3to5" name="q8"><label for="q8" class="light">4 to 5</label><br>          
          <input type="radio" id="q8" value="6to9" name="q8"><label for="q8" class="light">6 to 9</label><br>
		  <input type="radio" id="q8" value="10plus" name="q8"><label for="q8" class="light">10 or more</label><br>
		  
		 </fieldset>
			';			
		
			
		#questions 9 to end
			for($i = 9; $i<=17;$i++)
			{
			echo '
		  <fieldset id="question"'.$i.'>
          <label>'.$q[$i].'<br>
		  </label>
          <input type="radio" id="q'.$i.'" value="1" name="q'.$i.'"><label for="q'.$i.'" class="light">Completely Disagree</label><br>
          <input type="radio" id="q'.$i.'" value="2" name="q'.$i.'"><label for="q'.$i.'" class="light">Strongly Disagree</label><br>
          <input type="radio" id="q'.$i.'" value="3" name="q'.$i.'"><label for="q'.$i.'" class="light">Disagree</label><br>
		  <input checked type="radio" id="q'.$i.'" value="4" name="q'.$i.'"><label for="q'.$i.'" class="light">Neither agree nor disagree</label><br>
          <input type="radio" id="q'.$i.'" value="5" name="q'.$i.'"><label for="q'.$i.'" class="light">Agree</label><br>          
          <input type="radio" id="q'.$i.'" value="6" name="q'.$i.'"><label for="q'.$i.'" class="light">Strongly Agree</label><br>
		  <input type="radio" id="q'.$i.'" value="7" name="q'.$i.'"><label for="q'.$i.'" class="light">Completely Agree</label><br>
		  
		 </fieldset>
			';}			
			?>
        <button  TYPE="submit" name="submit">Next Step</button>
		<br><br>
					Progress:<br><br>
			<div id="progress">
			</div><br><?php echo $this->session->pagenum*10 ."%"?><br>
				  </form>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
			<style>
			#progress {
				background: #ffffff;
				border-radius: 25px;
				height: 40px;
				width: 100%;
				padding: 3px;
			}

			#progress:after {
				content: '';
				display: block;
				background: #5fcf80;
				width: <?php echo $this->session->pagenum*10 ."%"?>;
				height: 100%;
				border-radius: 25px;
			}
			</style>
	  <button onclick="alert(x.value);">check "debug"</button>
	  
	<script type="text/javascript">
		
		var radioGroupsCount = 17;
		var radioGroups = ["q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8", "q9", "q10", "q11","q12","q13","q14","q15","q16","q17"];

		function checkRadios()
		{
			
			var flag = true;
			
			for (var j = 0; j < 17; j++)
			{	
				var radios = document.getElementsByName(radioGroups[j])
				
				isThisChecked = 0;
				for (var i = 0; i < radios.length; i++) {
					
					if(radios[i].checked)
					{
					isThisChecked++;	
					}
					
				}
				console.log(isThisChecked);
				if (! isThisChecked)
				{flag=false;}
			}
			
			console.log("Are all radios checked? " + flag);
			
			if (flag == false)
			{swal("Please!", "Answer to all questions before proceeding!", "error");}
			return flag;
		}
	</script>

      <?php echo "current IPv6 is  ",$this->session->ip; ?> <br>
	  <?php echo "number of songs rated are:  ",$this->session->rated_songs; ?>
    </body>
</html>