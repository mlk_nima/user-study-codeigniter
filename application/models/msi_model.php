<?php
class msi_model extends CI_Model {
	
        public function __construct()
        {
                $this->load->database();
        }

		public function insert_msi()
		{
			$current_ip = $this->session->ip;
			$current_session_id = session_id();
			
			$this->load->helper('url');
			$data = array(
				'ip_address' => $this->session->ip,
				'msi1' => $this->input->post('q1'), 
				'msi2' => $this->input->post('q2'), 
				'msi3' => $this->input->post('q3'), 
				'msi4' => $this->input->post('q4'), 
				'msi5' => $this->input->post('q5'), 
				'msi6' => $this->input->post('q6'), 
				'msi7' => $this->input->post('q7'), 
				'msi8' => $this->input->post('q8'), 
				'msi9' => $this->input->post('q9'), 
				'msi10' => $this->input->post('q10'), 
				'msi11' => $this->input->post('q11'),
				'msi12' => $this->input->post('q12'),
				'msi13' => $this->input->post('q13'), 
				'msi14' => $this->input->post('q14'),
				'msi15' => $this->input->post('q15'), 
				'msi16' => $this->input->post('q16'), 
				'msi17' => $this->input->post('q17') 
			);

			$time = array('msi' => date("Y-m-d_h:i:sa"),'session_id' => $current_session_id);
			
			$this->db->update('time',$time,array('session_id' => $current_session_id));

			return $this->db->update('results', $data,array('session_id' => $current_session_id));

			
		}		
		
}