<?php
class ffm_model extends CI_Model {
	
        public function __construct()
        {
                $this->load->database();
        }

		public function insert_ffm()
		{
			$current_ip = $this->session->ip;
			$current_session_id = session_id();
			
			$this->load->helper('url');
			$data = array(
				'ip_address' => $this->session->ip,
				'ffm1' => $this->input->post('q1'), 
				'ffm2' => $this->input->post('q2'), 
				'ffm3' => $this->input->post('q3'), 
				'ffm4' => $this->input->post('q4'), 
				'ffm5' => $this->input->post('q5'), 
				'ffm6' => $this->input->post('q6'), 
				'ffm7' => $this->input->post('q7'), 
				'ffm8' => $this->input->post('q8'), 
				'ffm9' => $this->input->post('q9'), 
				'ffm10' => $this->input->post('q10') 
				);
				
			$time = array('ffm' => date("Y-m-d_h:i:sa"),'session_id' => $current_session_id);
			$this->db->update('time',$time,array('session_id' => $current_session_id));


			return $this->db->update('results', $data,array('session_id' => $current_session_id));

			
		}		
		
}