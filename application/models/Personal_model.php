<?php
class Personal_model extends CI_Model {
	
        public function __construct()
        {
                $this->load->database();
        }

		public function insert_personal()
		{
			
			
			$current_session_id = session_id();
			$this->load->helper('url');
			$data = array(
				'ip_address' => $this->session->ip,
				'age' => $this->input->post('user_age'),
				'gender' => $this->input->post('user_sex'),
				'education' => $this->input->post('user_edu'),
				'language' => $this->input->post('user_english'),
				'job' => $this->input->post('user_job'),
				'valence' => $this->input->post('valence'),
				'arousal' => $this->input->post('arousal'),
				'twitter' => $this->input->post('user_twitter'),
				'facebook' => $this->input->post('user_facebook')
			);
				   $time = array('personal' => date("Y-m-d_h:i:sa"),'session_id' => $current_session_id);
			
				   $this->db->update('time',$time,array('session_id' => $current_session_id));
			return $this->db->update('results', $data,array('session_id' => $current_session_id));
			
			
		}		
		
}