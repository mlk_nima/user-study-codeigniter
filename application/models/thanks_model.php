<?php
/*
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr<br>";
  echo "Your session has expired. If you wish, you can start over again.";
  echo "<br>";
  echo '<a href=".">Click here to start over</a>';  
  
  die();
}

//set error handler
set_error_handler("customError",E_NOTICE);
*/

class thanks_model extends CI_Model {
	
        public function __construct()
        {
                $this->load->database();
        }

		public function fetch_code()
		{
			$current_ip = $this->session->ip;
			$current_session_id = session_id();
			
			$date = new DateTime();
			$a =  $date->format('mYdHis');
			$b = rand(10,99);
			$b = (string)$b;
			$c = "23";
			$code = $b . $a . $c;

			$this->load->helper('url');
			$insertdata = array(
				'ip_address' => $this->session->ip,
				'code' => $code
				);
			
			
			$exists = 0;
			#$query = $this->db->get_where('results', array('session_id' => session_id(), 'code' => NULL));#->result();
			#$query = $this->db->select('code')->where('session_id',session_id())->get('results');
			#$query = $this->db->query('SELECT code FROM results WHERE session_id='.session_id());

			$this->db->select('code');
			$this->db->from('results');
			$this->db->where('session_id',session_id());
			$query = $this->db->get();
						
			if (is_null($query->row()->code)) {
				$exists = 0;
			} else {
				$exists = 1;
			}
			#return $exists;
			
			if ($exists == 1)
			{return $query->row()->code;}else
			{
			$this->db->update('results', $insertdata,array('session_id' => $current_session_id));
			return $code;
			}
			

		}		
		
}