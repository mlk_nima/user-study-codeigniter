<?php
class Ratings_model extends CI_Model {
	
        public function __construct()
        {
                $this->load->database();
        }

		public function insert_ratings()
		{
			$current_ip = $this->session->ip;
			$current_session_id = session_id();
			
			$this->load->helper('url');
			
			$score = 'score'; 
			$song1 = 'song1_';
			$song2 = 'song2_';
			$score .= $this->session->rated_songs+1;
			$song1 .= $this->session->rated_songs+1;
			$song2 .= $this->session->rated_songs+1;
			
			$data = array(
				'ip_address' => $this->session->ip,
				$score => $this->input->post('rating'),
				$song1 => $this->input->post('rnd1'),
				$song2 => $this->input->post('rnd2') 
				);


			$this->session->set_userdata('rated_songs', $this->session->rated_songs +1);
			#$this->db->update('results',$data,array('session_id' => $current_session_id));

			$data = array(
				'session_id' => $current_session_id,
				'score' => $this->input->post('rating'),
				'song1' => $this->input->post('rnd1'),
				'song2' => $this->input->post('rnd2')
				
				);			
			
			return $this->db->insert('scores',$data);
			
			
			
		}		
		
}