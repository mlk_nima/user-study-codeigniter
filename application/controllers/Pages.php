<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pages extends CI_Controller {

        public function start()
        {
        if ( ! file_exists(APPPATH.'views/pages/start.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
				
        }
		session_regenerate_id();
        $this->load->view('pages/start');
		#$this->load->helper('url');
	
        }
		
		public function personal()
        {
        if ( ! file_exists(APPPATH.'views/pages/personal.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
				
        }
		
		$this->session->pagenum=0.5;

		$this->load->helper('form');
		$this->load->library('form_validation');
       
		$this->session->set_userdata('ip', $_SERVER['REMOTE_ADDR']);
		
		$this->load->model('start_model');
        $this->start_model->insert_ip_address();
		
	    $this->load->view('pages/personal');
		$this->load->helper('url');
		
		$this->session->been_alerted = 0;

	
        }
		
		public function songs()
        {
        /*if ($this->session->rated_songs == 5)
        {
                // Enough songs have been rated
                redirect('pages/ffm');
				
        }*/
		$this->session->pagenum=2.5;

		$song1_url = "audio/song (1).mp3";
		$song2_url = "audio/song (2).mp3";

		// function fetchNextPair($mode) {
			// if ($mode=="random")
			// {
				#read the bin file:
				$file = fopen(base_url()."assets\statistics\binForPHP.csv","r");
				$i=1;
				while(! feof($file))
				  {
					$bin[$i]=fgetcsv($file);
					$i++;
				  }
				fclose($file);
				
				
				global $song1_url, $song2_url;
				//$rnd1 = mt_rand(1,200);
				$rnd1 = implode($bin[mt_rand(0, count($bin) - 1)]);
				$song1_url = "audio/song ($rnd1).mp3";
				//$rnd2 = mt_rand(1,200);
				do {
					$rnd2 = implode($bin[mt_rand(0, count($bin) - 1)]);
				} while ($rnd2 == $rnd1);
				$song2_url = "audio/song ($rnd2).mp3";
		
		
		$data = array(
		'song1' => $song1_url,
		'song2' => $song2_url,
		'rnd1' => $rnd1,
		'rnd2' => $rnd2,
		'rated_songs' => $this->session->rated_songs
		);
		
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->load->view('pages/songs',$data);
		$this->load->helper('url');
	
        }
		
		public function ffm()
		{
		if ( ! file_exists(APPPATH.'views/pages/ffm.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
				
        }	
		$this->session->pagenum=5.5;

		
		$time = array('songs' => date("Y-m-d_h:i:sa"),'session_id' => session_id());	
		$this->db->update('time',$time,array('session_id' => session_id()));

		$this->load->view('pages/ffm');
		}
		
		public function msi()
		{
		if ( ! file_exists(APPPATH.'views/pages/msi.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
				
        }	
		$this->session->pagenum=7.5;

		$this->load->view('pages/msi');
		}
		
		public function thanks()
		{
		if ( ! file_exists(APPPATH.'views/pages/msi.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
				
        }	
		$this->load->view('pages/thanks');
		}
}