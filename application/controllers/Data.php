<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Data extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('personal_model');
				$this->load->model('personal_model');
                $this->load->helper('url_helper');
        }


        public function personal()
        {
		$this->load->model('personal_model');

		$this->session->set_userdata('rated_songs', 0);
        $this->personal_model->insert_personal();
		redirect('/pages/songs');

        }
		

        public function ratings()
        {
		$this->load->model('ratings_model');
        $this->ratings_model->insert_ratings();
		redirect('/pages/songs');
        }
		
		public function ffm()
        {
		$this->load->model('ffm_model');
        $this->ffm_model->insert_ffm();
		redirect('/pages/msi');

        }
		
		public function msi()
        {
		$this->load->model('msi_model');
        $this->msi_model->insert_msi();
		redirect('/data/thanks');

        }
		
		public function thanks()
        {
		$this->session->pagenum=10;
		
		$this->load->model('thanks_model');
        $data['code'] = $this->thanks_model->fetch_code();
		$this->load->view('pages/thanks', $data);
		#$this->session->sess_destroy();

        }

}
