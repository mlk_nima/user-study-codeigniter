![alt tag](assets/icon/icon.png)

# User Study Using Pairwise Preferences

Improvements:
==============

21.01.2017
-------
* The time of submission of each page is now kept in separate table "time".
* The interface for Valence-Arousal values is created. It works and also the values (-100,100)are stored in the DB.
* The Valence-Arousal interface now shows the point clicked by the user.
* Implemented the fetching function using the probability distribution function derived form the last study (ministudy).
* Data storage for Language proficiency and occupation.
* Implemented and tested the full Musical Sophistication Index page, plus their data storage (integers values instead of text responses, as suggested by the designers of the survey).


18.01.2017
-------
* The text for song's elapsed seconds is corrected.
* An animation for the comparison page that indicates that the score is recorded.
* A simple progress bar on the bottom of pages. Since the number of songs to be rated is not definite, it is an estimation.
* Added language proficiency and occupation questions.
* Added a simple yet functional interface for Valence-Arousal values.
* Enhanced alerts instead of stock js alerts.

13.01.2017
-------
* The refresh issue in the last page is now fixed. Only one code is generated for a user. When the session expires after 15 minutes, the error is handled and the user will receive a custom message and is presented with the option to start over again.
* The pairwise scores are now stored in another table called 'scores'.
* The I'm done rating button only appears after at least N pairs of songs have been rated. For now N is set to 2.
* The user is prompted about the possibility to keep rating more songs or going forward to the next session when it is relevant.
* Animation for the audio buttons.
* The FFM questions are now inserted into their page correctly.
* "Don't really care" text is removed from the slider's legend.

10.01.2017
-------
* The database driver is now changed to SQLite3.
* Each time the starting page is visited, a new session is created, hence a new data row is inserted. In other words, no one can edit his/her answers.
* Audio Player buttons are redesigned and look more like an actual player now.
* Questions of the survey are now in a separate php file, it suffices to change them to see the effect in the webpages.
* Back button is disabled on all pages.
* phpLiteMyAdmin is installed in the same folder as the database to manage and view the DB. It is accessible at: sqlitephp/phpliteadmin.php using the default password found in inside the php file.

05.01.2017
-------
* Slider bar now shows the value immediately, instead of after button release.
* Pages regarding FFM and MSI are validated upon submit.

Older
----
* The field "Gender" is added to the personal page.
* Slider bar width and labels look fine across popular platforms.
* Audio contorls are redesigned and are fully compatible.
* all pages are functioning with the correct flow.
* Inserting to DB is OK for all five pairs of songs.
* all pages insert correctly. (also final code for AMT)
* Javascripts on songs.php functioning flawlessly.
* Redirects are OK using redirect() in controllers.
* All pages (update) instead of inserting into a new row.
* Session data is used and works well.
* An SQLite database is created in /assets/sqlitedb.

To Consider:
====
* The Songs page fetches new songs randomly. Need to come up with an idea
* Need to define the minimum number of songs to listen to.
* Regarding MSI, we need to decide on the survey questions and edit the question files accordingly.

Before going online:
====
* The minimum time of listening to each song is set to 2 seconds, mainly for debugging reasons.
* Minimum number of songs should be set properly.
* All radio buttons on FFM and MSI pages are pre-selected except the 10th question to speed up debugging. Need to be cleared when replacing the fake text with the actual questions.
- Need to make a final touch on the views before going online.
- Need to remove the texts and buttons used for debugging from views. (final step)
- Need to move js logic out of the pages regarding Songs, MSI, and FFM. (final step)