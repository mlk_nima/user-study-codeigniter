
//<![CDATA[

      // SDK Needs to create video and canvas nodes in the DOM in order to function
      // Here we are adding those nodes a predefined div.
      var divRoot = $("#affdex_elements")[0];
      var width = 640;
      var height = 480;
      var faceMode = affdex.FaceDetectorMode.LARGE_FACES;
      //Construct a CameraDetector and specify the image width / height and face detector mode.
      var detector = new affdex.CameraDetector(divRoot, width, height, faceMode);

      //Enable detection of all Expressions, Emotions and Emojis classifiers.
      detector.detectAllEmotions();
      //detector.detectAllExpressions();
      //detector.detectAllEmojis();
      //detector.detectAllAppearance();

      //Add a callback to notify when the detector is initialized and ready for runing.
      detector.addEventListener("onInitializeSuccess", function() {
        log('#logs', "Initialized");
        //Display canvas instead of video feed because we want to draw the feature points on it
        $("#face_video_canvas").css("display", "block");
        $("#face_video").css("display", "none");
      });

      function log(node_name, msg) {
        $(node_name).append("<span>" + msg + "</span><br />")
        //$.get(
          //  "storeEmotions.php",
            //{message: msg}, 
            //function(data,status){},
            //"html");
      }

      //function executes when Start button is pushed.
      function onStart() {
        if (detector && !detector.isRunning) {
          $("#logs").html("");
          detector.start();
        }
        log('#logs', "Started");
      }

      //function executes when the Stop button is pushed.
      function onStop() {
        log('#logs', "Ended");
        if (detector && detector.isRunning) {
          detector.removeEventListener();
          detector.stop();
        }
      };

      //function executes when the Reset button is pushed.
      function onReset() {
        log('#logs', "Reset");
        if (detector && detector.isRunning) {
          detector.reset();

          $('#results').html("");
        }
      };

      //Add a callback to notify when camera access is allowed
      detector.addEventListener("onWebcamConnectSuccess", function() {
        log('#logs', "Allowed");
      });

      //Add a callback to notify when camera access is denied
      detector.addEventListener("onWebcamConnectFailure", function() {
        log('#logs', "Denied");
        console.log("Webcam access denied");
      });

      //Add a callback to notify when detector is stopped
      detector.addEventListener("onStopSuccess", function() {
        log('#logs', "SuccessStop");
        $("#results").html("");
      });

      //Add a callback to receive the results from processing an image.
      //The faces object contains the list of the faces detected in an image.
      //Faces object contains probabilities for all the different expressions, emotions and appearance metrics
      detector.addEventListener("onImageResultsSuccess", function(faces, image, timestamp) {
        
        if (faces.length > 0) {

		  jsonEmotions = JSON.stringify(faces[0].emotions);
		  var parsedEmotions = JSON.parse(jsonEmotions);
		  
		  var _faces = faces.length;
		  var _joy = Math.round(parsedEmotions.joy);
		  var _sadness = Math.round(parsedEmotions.sadness);
		  var _disgust = Math.round(parsedEmotions.disgust);
		  var _contempt = Math.round(parsedEmotions.contempt);
		  var _anger = Math.round(parsedEmotions.anger);
		  var _fear = Math.round(parsedEmotions.fear);
		  var _surprise = Math.round(parsedEmotions.surprise);
		  var _valence = Math.round(parsedEmotions.valence);
		  var _engagement = Math.round(parsedEmotions.engagement);
		  
		  console.log(_joy);
		  
		    $.get(
            "storeEmotions.php",
            {	
				time: timestamp.toFixed(2),
				faces: _faces,
				joy: _joy,
				sadness: _sadness,
				disgust: _disgust,
				contempt: _contempt,
				anger: _anger,
				fear: _fear,
				surprise: _surprise,
				valence: _valence,
				engagement: _engagement,
			}, 
            function(data,status){},
            "html");
        }
      });

      //Draw the detected facial feature points on the image
      function drawFeaturePoints(img, featurePoints) {
        var contxt = $('#face_video_canvas')[0].getContext('2d');

        var hRatio = contxt.canvas.width / img.width;
        var vRatio = contxt.canvas.height / img.height;
        var ratio = Math.min(hRatio, vRatio);

        contxt.strokeStyle = "#FFFFFF";
        for (var id in featurePoints) {
          contxt.beginPath();
          contxt.arc(featurePoints[id].x,
            featurePoints[id].y, 2, 0, 2 * Math.PI);
          contxt.stroke();

        }
      }

//]]> 

  // tell the embed parent frame the height of the content
  if (window.parent && window.parent.parent){
    window.parent.parent.postMessage(["resultsFrame", {
      height: document.body.getBoundingClientRect().height,
      slug: "opyh5e8d"
    }], "*")
  }
