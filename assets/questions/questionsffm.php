<?php
$title = "Here are a number of personality traits that may or may not apply to you.
Please choose an option for each statement to indicate the extent to which you
agree or disagree with that statement. You should rate the extent to which the pair
of traits applies to you, even if one characteristic applies more strongly than the
other.";

$q1 = "1. Extraverted, enthusiastic.";
$q2 = "2. Critical, quarrelsome.";
$q3 = "3. Dependable, self-disciplined.";
$q4 = "4. Anxious, easily upset.";
$q5 = "5. Open to new experiences, complex.";
$q6 = "6. Reserved, quiet.";
$q7 = "7. Sympathetic, warm.";
$q8 = "8. Disorganized, careless.";
$q9 = "9. Calm, emotionally stable.";
$q10 = "10. Conventional, uncreative.";
?>