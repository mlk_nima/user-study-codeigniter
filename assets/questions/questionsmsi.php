<?php
$title = "Here you can find 17 questions reagrding your musical abilities.";
$q[1] = "1. I spend a lot of my free time doing music-related activities.";
$q[2] = "2. I often read or search the internet for things related to music.";
$q[3] = "3. Music is kind of an addiction for me I couldn’t live without it.";
#-------------------------
$q[4] = "4. I am able to judge whether someone is a good singer or not.";
$q[5] = "5. I find it difficult to spot mistakes in a performance of a song even if I know the tune.";
$q[6] = "6. When I sing, I have no idea whether I’m in tune or not.";
#-------------------------
$q[7] = "7. I engaged in regular, daily practice of a musical instrument (including voice) for ----- years."; #  0 / 1 / 2 / 3 / 4-5 / 6-9 / 10 or More
$q[8] = "8. I have had ----- years of formal training on a musical instrument (including voice) during my lifetime."; # 0 / 0.5 / 1 / 2 / 3-5 / 6-9 / 10 or more
$q[9] = "9. I would not consider myself a musician.";
#-------------------------
$q[10] = "10. I am able to hit the right notes when I sing along with a recording.";
$q[11] = "11. I don’t like singing in public because I’m afraid that I would sing wrong notes.";
$q[12] = "12. I only need to hear a new tune once and I can sing it back hours later.";
#-------------------------
$q[13] = "13. I am able to talk about the emotions that a piece of music evokes for me.";
$q[14] = "14. I often pick certain music to motivate or excite me.";
$q[15] = "15. I sometimes choose music that can trigger shivers down my spine.";
$q[16] = "16. Music can evoke my memories of past people and places.";
$q[17] = "17. Pieces of music rarely evoke emotions for me.";
?>